package br.com.alugapay.api.resourceserver.response;

public class UserRest {
    private String idserFirtName;
    private String userLastName;
    private String userId;

    public UserRest(String idserFirtName, String userLastName, String userId) {
        this.idserFirtName = idserFirtName;
        this.userLastName = userLastName;
        this.userId = userId;
    }

    public String getIdserFirtName() {
        return idserFirtName;
    }

    public void setIdserFirtName(String idserFirtName) {
        this.idserFirtName = idserFirtName;
    }

    public String getUserLastName() {
        return userLastName;
    }

    public void setUserLastName(String userLastName) {
        this.userLastName = userLastName;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
